import { TestBed } from '@angular/core/testing';
import { take } from 'rxjs';

import { LogService } from './log.service';

describe('LogService', () => {
  let service: LogService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LogService],
    });
    service = TestBed.inject(LogService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('test Test method', () => {
    expect(service.test()).toBe('TEST');
  });

  it('get data method', () => {
    const input = 3;
    const output = [4, 5, 6];
    service
      .getData(input)
      .pipe(take(1))
      .subscribe((data) => expect(data).toEqual(output));
  });
});
