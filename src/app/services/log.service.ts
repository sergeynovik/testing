import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable()
export class LogService {
  public write(data: any): void {
    console.log(data);
  }

  public test(): string {
    return 'TEST';
  }

  public getData(startNumber: number): Observable<number[]> {
    return of([startNumber + 1, startNumber + 2, startNumber + 3]);
  }
}
