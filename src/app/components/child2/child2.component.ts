import { LogService } from './../../services/log.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-child2',
  templateUrl: './child2.component.html',
  styleUrls: ['./child2.component.scss'],
})
export class Child2Component implements OnInit {
  constructor(public log: LogService) {}

  ngOnInit(): void {}
}
