import { Child2Component } from './child2/child2.component';
import { ChildComponent } from './child/child.component';
import { NgModule } from '@angular/core';

const components = [ChildComponent, Child2Component];

@NgModule({
  imports: [],
  exports: [...components],
  declarations: [...components],
  providers: [],
})
export class ComponentsModule {}
