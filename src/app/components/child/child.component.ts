import { LogService } from './../../services/log.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.scss'],
  providers: [LogService],
  // viewProviders: [LogService],
})
export class ChildComponent implements OnInit {
  constructor(public log: LogService) {}

  ngOnInit(): void {
    this.log.write('Hi');
  }
}
