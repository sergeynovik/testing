import { browser, by, element, protractor } from 'protractor';

describe('Main', () => {
  beforeEach(async () => {
    browser.restart();
  });

  it('Open main page and search title ', async () => {
    let EC = protractor.ExpectedConditions;
    await browser.get('http://localhost:4200');
    let title = element(by.css('.content span'));
    await browser.wait(EC.presenceOf(title), 5000);
  });
});
